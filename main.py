from subprocess import PIPE, run
from time import sleep
from os import system

def checkBattery():
    while 1:
        batteryPercentage = run("upower --dump | grep percentage", stdout=PIPE, stderr=PIPE, universal_newlines=True, shell=True)
        batteryPercentage =  batteryPercentage.stdout.split('\n')[0].split(':')[1].strip().strip('%')

        chargingStatus = run("upower --dump | grep online", stdout=PIPE, stderr=PIPE, universal_newlines=True, shell=True)
        chargingStatus =  chargingStatus.stdout.split(":")[1].strip()

        if int(batteryPercentage) < 40 and chargingStatus == 'no':
            system("aplay sound/plugIn.wav")

        if int(batteryPercentage) > 80 and chargingStatus == 'yes':
            system("aplay sound/plugOut.wav")
        
        sleep(30)

if __name__ == "__main__":
    checkBattery()
